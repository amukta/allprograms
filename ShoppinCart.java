import java.util.*;
import java.util.stream.Collectors;


public class ShoppinCart {

    public static void main(String args[]){
        Map<String,ArrayList> owner = new HashMap<String,ArrayList>();
        ArrayList<String> zara = new ArrayList<>();
        ArrayList<String> puma = new ArrayList<>();
        ArrayList<String> max = new ArrayList<>();
        boolean display;
        zara.add("shoes");
        zara.add("handbags");
        puma.add("shoes");
        puma.add("perfumes");
        puma.add("shirts");
        puma.add("handbags");
        max.add("shoes");
        max.add("jewellery");
        max.add("handbags");
        max.add("perfumes");
        max.add("kurtas");
        owner.put("zarabrand",zara);
        owner.put("pumabrand",puma);
        owner.put("maxbrand",max);
        zara.add("kurtas") ;
        for(Map.Entry e: owner.entrySet())
            System.out.println(e.getKey() + " " + e.getValue());
        Map<String,ArrayList> cart = new HashMap<String,ArrayList>();
        ArrayList<String> zaracust= new ArrayList<>();
        ArrayList<String> pumacust = new ArrayList<>();
        zaracust.add("shoes");
        pumacust.add("perfumes");
        cart.put("zara", zaracust);
        cart.put("puma",pumacust);
        System.out.println("****************** Customer Cart *****************");
        for(Map.Entry e: cart.entrySet())
            System.out.println(e.getKey() + " " + e.getValue());

    }
}
