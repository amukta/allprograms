import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;

public class Prime {
    public static void main(String[] args){
        boolean flag = false;
        System.out.println("Enter non-zero number to be checked: ");
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();

            for(int i=2;i<=input/2;i++){
                if(input%i==0)
                 flag = true; break;
        }
            if(flag) {
                System.out.println( input + " is not prime");
            }
            else if(input==1){
                System.out.println("neither prime nor composite");
            }
        else System.out.println(input + " is a prime");

    }
}
