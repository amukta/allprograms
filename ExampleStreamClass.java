import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class ExampleStreamClass {
    public static void main(String[] args){
        List<String> inputs = Arrays.asList("abc","bcd","defg","jk");
        System.out.println(inputs.stream().filter(i -> i.length()<2 ).collect(Collectors.toList()));

    }
}
